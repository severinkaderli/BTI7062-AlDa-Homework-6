package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The class is used to display results from an CSV file as
 * a nice looking JavaFX chart.
 *
 * @author Severin Kaderli
 */
public class Gui extends Application {
    /**
     * The chart which will be displayed in the end.
     */
    LineChart<Number, Number> chart;

    @Override
    public void start(Stage primaryStage) {
        System.out.println("Running. Please check that data is provided via STDIN!");

        // Create the chart
        NumberAxis xAxis = new NumberAxis();
        NumberAxis yAxis = new NumberAxis();
        this.chart = new LineChart<>(xAxis, yAxis);
        this.chart.setTitle("Quick Sort DNC Comparison");

        // Get and add the series data
        List<XYChart.Series> seriesList = this.getSeries();
        for (XYChart.Series series : seriesList) {
            this.chart.getData().add(series);
        }

        // Prepare the axes
        xAxis.setLabel("Number of sorted elements");
        xAxis.setAutoRanging(false);

        // Setting bounds and tick unit based on the values
        xAxis.setLowerBound(this.chart
                .getData()
                .get(0)
                .getData()
                .get(0)
                .getXValue()
                .intValue());
        xAxis.setUpperBound(this.chart
                .getData()
                .get(0)
                .getData()
                .get(this.chart.getData().get(0).getData().size() - 1).getXValue().intValue());
        xAxis.setTickUnit(this.chart
                .getData()
                .get(0)
                .getData()
                .get(1)
                .getXValue()
                .intValue() - this.chart.getData().get(0).getData().get(0).getXValue().intValue());
        yAxis.setLabel("Execution time [ms]");

        this.prepareTooltips();
        this.displayChart(primaryStage);
    }


    /**
     * Gets a CSV string from the standard input
     *
     * @return The CSV string
     */
    private String getCsvFromInput() {
        StringBuilder csvStringBuilder = new StringBuilder();

        try {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));

            String line;
            while ((line = buffer.readLine()) != null) {
                csvStringBuilder.append(line);
                csvStringBuilder.append("\n");
            }

        } catch (IOException e) {
            throw new RuntimeException("IOException");
        }

        if (csvStringBuilder.toString().equals("")) {
            throw new RuntimeException("Empty input");
        }

        return csvStringBuilder.toString();
    }

    /**
     * Get the series based on the input CSV data.
     *
     * @return A list of the series
     */
    private List<XYChart.Series> getSeries() {
        String csvString = this.getCsvFromInput();
        List<String> csvRows = Arrays.asList(csvString.split("\n"));
        List<XYChart.Series> seriesList = new ArrayList<>();

        for (int i = 0; i < csvRows.size(); i++) {
            List<String> rowValues = Arrays.asList(csvRows.get(i).split(";"));

            if (i == 0) {
                for (int j = 1; j < rowValues.size(); j++) {
                    XYChart.Series series = new XYChart.Series();
                    series.setName(rowValues.get(j));
                    seriesList.add(series);
                }
                continue;
            }

            Double xValue = Double.valueOf(rowValues.get(0));

            for (int j = 1; j < rowValues.size(); j++) {
                seriesList.get(j - 1).getData().add(new XYChart.Data<>(xValue, Double.valueOf(rowValues.get(j))));
            }
        }

        return seriesList;
    }

    /**
     * Sets the tooltips of the data points in the chart based
     * on the values.
     */
    private void prepareTooltips() {
        for (XYChart.Series<Number, Number> s : this.chart.getData()) {
            float avgms = 0;
            for (XYChart.Data<Number, Number> d : s.getData()) {
                Tooltip tooltip = new Tooltip(String.format("Sorted %d elements in %.2fms%n%s", d.getXValue().intValue(), d.getYValue().doubleValue(), s.getName()));
                Tooltip.install(d.getNode(), tooltip);
                avgms += d.getYValue().floatValue();
            }
            avgms /= s.getData().size();
            s.setName(s.getName() + String.format("(∅ %.2fms)", avgms));
        }
    }

    /**
     * Displays the chart on the given stage
     *
     * @param stage The stage
     */
    private void displayChart(Stage stage) {
        Scene scene = new Scene(chart, 800, 600);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
