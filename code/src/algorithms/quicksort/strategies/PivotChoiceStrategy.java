package algorithms.quicksort.strategies;

import java.util.List;

public class PivotChoiceStrategy {
    public static final PivotChoiceStrategy INSTANCE = new PivotChoiceStrategy();

    public Long pickPivot(List<Long> input) {
        return (input.get(0) +
                input.get(input.size() / 2) +
                input.get(input.size() - 1)) / 3;
    }

    @Override
    public String toString() {
        return "BasicPivot";
    }
}
