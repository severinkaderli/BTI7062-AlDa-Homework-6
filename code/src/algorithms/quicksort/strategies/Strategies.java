package algorithms.quicksort.strategies;

public class Strategies {
    private PivotChoiceStrategy pivotChoiceStrategy;

    public Strategies() {
        this(PivotChoiceStrategy.INSTANCE);
    }

    public Strategies(PivotChoiceStrategy pivotChoiceStrategy) {
        this.pivotChoiceStrategy = pivotChoiceStrategy;
    }

    public PivotChoiceStrategy pivotChoiceStrategy() {
        return this.pivotChoiceStrategy;
    }
}
