package algorithms.quicksort;

import algorithms.quicksort.strategies.Strategies;
import algorithms.templates.ThreadedDivideAndConquerable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QuickSortThreadedDNC implements ThreadedDivideAndConquerable<List<Long>> {

    private List<Long> input;

    private Strategies strategies;

    public QuickSortThreadedDNC(List<Long> input) {
        this(input, new Strategies());
    }

    public QuickSortThreadedDNC(List<Long> input, Strategies strategies) {
        this.input = input;
        this.strategies = strategies;
    }

    @Override
    public boolean isBasic() {
        return input.size() <= 2;
    }

    @Override
    public List<Long> baseFun() {
        if (input.size() < 2 || input.get(0) < input.get(1)) {
            return input;
        }
        return Arrays.asList(input.get(1), input.get(0));
    }

    @Override
    public List<? extends ThreadedDivideAndConquerable<List<Long>>> decompose() {
        List<Long> left = new ArrayList<>(input.size() / 2);
        List<Long> right = new ArrayList<>(input.size() / 2);
        Long pivot = strategies.pivotChoiceStrategy().pickPivot(input);
        for (Long value : input) {
            if (value < pivot) {
                left.add(value);
            } else {
                right.add(value);
            }
        }
        List<QuickSortThreadedDNC> subcomponents = new ArrayList<>(2);
        subcomponents.add(new QuickSortThreadedDNC(left, this.strategies));
        subcomponents.add(new QuickSortThreadedDNC(right, this.strategies));

        return subcomponents;
    }

    @Override
    public boolean isThreadable() {
        return input.size() > 20;
    }

    @Override
    public List<Long> recombine(List<List<Long>> intermediateResults) {
        return Stream.concat( //
                intermediateResults.get(0).stream(), //
                intermediateResults.get(1).stream()) //
                .collect(Collectors.toList()); //
    }
}
