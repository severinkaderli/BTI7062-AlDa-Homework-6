package benchmark;

import algorithms.quicksort.QuickSortDNC;
import algorithms.quicksort.QuickSortThreadedDNC;
import benchmark.utils.DataProvider;
import benchmark.utils.DurationTimer;
import benchmark.utils.config.QuickSortConfig;
import benchmark.utils.io.ArgParser;
import benchmark.utils.io.CsvExporter;
import benchmark.utils.io.Logger;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Supplier;

public class Benchmark {
    private static QuickSortConfig config;
    private static DataProvider dataProvider;
    private static Logger logger;

    public static void main(String[] args) {
        ArgParser parser = new ArgParser();
        config = parser.parse(args).buildQuickSortConfig();

        dataProvider = DataProvider.ensure(config);
        logger = Logger.ensure(config);

        logger.verbose("Starting data gathering");

        Map<String, Supplier<List<BenchmarkDataSeries>>> benchmarks = new HashMap<>();
        benchmarks.put("Java sorting algorithm", () -> getJavaSeries());
        benchmarks.put("SimpleDNC sorting algorithm", () -> getSimpleSeries());
        benchmarks.put("Multithreaded sorting algorithm", () -> getThreadedSeries());

        List<BenchmarkDataSeries> data = runBenchmarks(benchmarks);
        logger.debug("Dumping results as CSV to std::out...");
        System.out.print(CsvExporter.dataToCSV(data));
        System.exit(0);
    }

    private static List<BenchmarkDataSeries> runBenchmarks(Map<String, Supplier<List<BenchmarkDataSeries>>> algorithms) {
        List<BenchmarkDataSeries> benchmarkData = new ArrayList<>();

        for (Map.Entry<String, Supplier<List<BenchmarkDataSeries>>> algorithm : algorithms.entrySet()) {
            logger.verbose("Benchmarking %s...", algorithm.getKey());
            for (int i = 0; i < config.getNumberOfWarmupIterations(); i++) {
                logger.verbose("\tWarmup Iteration #%d of %d", i + 1, config.getNumberOfWarmupIterations());
                algorithm.getValue().get();
            }
            logger.verbose("\tRunning benchmark now!");
            benchmarkData.addAll(algorithm.getValue().get());
            logger.verbose("\tDone!");
        }

        return benchmarkData;
    }

    private static List<BenchmarkDataSeries> getSimpleSeries() {
        DurationTimer timer = new DurationTimer();
        BenchmarkDataSeries series = new BenchmarkDataSeries("Simple DNC");
        for (int i = config.getLowerBound(); i <= config.getUpperBound(); i += config.getStepSize()) {
            logger.debug("\t\tSorting %d values", i);
            List<Long> toSort = dataProvider.get(i);
            timer.start();
            QuickSortDNC sort = new QuickSortDNC(toSort, config.getStrategies());
            check(sort.divideAndConquer());
            timer.stop();
            series.addElement(i, timer.getDuration());
        }

        return Arrays.asList(series);
    }

    private static List<BenchmarkDataSeries> getJavaSeries() {
        DurationTimer timer = new DurationTimer();
        BenchmarkDataSeries series = new BenchmarkDataSeries("Collections.sort(...)");
        for (int i = config.getLowerBound(); i <= config.getUpperBound(); i += config.getStepSize()) {
            logger.debug("\t\tSorting %d values", i);
            List<Long> toSort = dataProvider.get(i);
            timer.start();
            Collections.sort(toSort);
            timer.stop();
            series.addElement(i, timer.getDuration());
        }

        return Arrays.asList(series);
    }

    private static List<BenchmarkDataSeries> getThreadedSeries() {
        List<BenchmarkDataSeries> allSeries = new ArrayList<>();

        for (int threadCount : config.getThreadCounts()) {
            ForkJoinPool pool = new ForkJoinPool(threadCount);

            logger.verbose("\t\tCalculating with %d threads...", threadCount);
            DurationTimer timer = new DurationTimer();
            BenchmarkDataSeries series = new BenchmarkDataSeries(String.format("Threaded DNC (%d threads)", threadCount));
            for (int i = config.getLowerBound(); i <= config.getUpperBound(); i += config.getStepSize()) {
                logger.debug("\t\t\tSorting %d values", i);
                List<Long> toSort = dataProvider.get(i);
                timer.start();
                QuickSortThreadedDNC sort = new QuickSortThreadedDNC(new Vector<>(toSort), config.getStrategies());
                check(sort.divideAndConquer(pool));
                timer.stop();
                series.addElement(i, timer.getDuration());
            }
            allSeries.add(series);
        }
        return allSeries;
    }

    private static void check(List<Long> result) {
        if (config.getCheckCorrectness()) {
            logger.debug("\t\tChecking results of length %d for correctness...", result.size());
            List<Long> sorted = new ArrayList<>(result);
            Collections.sort(sorted);
            if (!sorted.equals(result)) {
                throw new IllegalArgumentException("not actually sorted!");
            }
        }
    }
}
