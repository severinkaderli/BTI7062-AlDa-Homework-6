package benchmark;

import java.util.HashMap;
import java.util.Map;

public class BenchmarkDataSeries {
    private final String name;
    private final Map<Number, Number> values = new HashMap<>();

    public BenchmarkDataSeries(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public Map<Number, Number> getData(){
        return this.values;
    }

    public void addElement(Integer n, Double ms){
        this.values.put(n, ms);
    }
}
