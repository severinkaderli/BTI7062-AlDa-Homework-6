package benchmark.utils;

public class DurationTimer {

    long startTime;
    long endTime;

    public DurationTimer() {
        this.startTime = 0;
        this.endTime = 0;
    }

    public void start() {
        this.startTime = System.nanoTime();
    }

    public void stop() {
        this.endTime = System.nanoTime();
    }

    /**
     * Gets the duration in ms.
     *
     * @return The duration of the timer in ms.
     */
    public double getDuration() {
        return (double) (this.endTime - this.startTime) / 1_000_000;
    }
}
