package benchmark.utils.io;

import benchmark.BenchmarkDataSeries;
import benchmark.utils.config.Config;
import javafx.scene.chart.XYChart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CsvExporter {
    public static String dataToCSV(List<BenchmarkDataSeries> data) {
        List<String> headers = new ArrayList<>(data.size() + 1);
        headers.add("size");
        for (BenchmarkDataSeries series : data) {
            headers.add(series.getName());
        }

        int sampleSize = data.get(0).getData().size();

        List<String> rows = new ArrayList<>(sampleSize);

        for(Map.Entry<Number, Number> entry : data.get(0).getData().entrySet()){
            List<Number> row = new ArrayList<>();
            row.add(entry.getKey());
            row.add(entry.getValue());
            for(BenchmarkDataSeries run : data.subList(1, data.size())){
                row.add(run.getData().get(entry.getKey()));
            }
            rows.add(row.stream().map(n -> String.format("%.3f", n.doubleValue())).collect(Collectors.joining(";")));
        }

        Collections.sort(rows, (a, b) -> {
            Double av = Double.valueOf(a.split(";")[0]);
            Double bv = Double.valueOf(b.split(";")[0]);
            return av.compareTo(bv);
        });

        String header = headers.stream().collect(Collectors.joining(";"));
        rows.add(0, header);

        return String.join("\n", rows);
    }
}
