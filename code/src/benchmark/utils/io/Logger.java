package benchmark.utils.io;

import benchmark.utils.config.Config;

import java.util.HashMap;
import java.util.Map;

import static benchmark.utils.io.Logger.Level.*;

/**
 * Provides Configurable log levels
 * debug, verbose, quiet
 */
public class Logger {
    private final Config config;
    private static Map<Config, Logger> instances = new HashMap<>();
    private static final String LOG_INDICATOR = "#";

    /**
     * returns or creates Logger instance from a map, ensuring you get the right config.
     */
    public static Logger ensure(Config config) {
        return instances.computeIfAbsent(config, Logger::new);
    }

    private Logger(Config config) {
        this.config = config;
    }

    /**
     * Logs if level is set to debug or more
     * Takes args like String.format
     */
    public void debug(String output, Object... args) {
        this.debug(String.format(output, args));
    }

    /**
     * Logs if level is set to debug or more
     * Takes args like String.format
     */
    public void debug(String output) {
        if (shouldLog(DEBUG)) {
            System.out.println(LOG_INDICATOR + output);
        }
    }

    /**
     * Logs if level is set to verbose or more
     * Takes args like String.format
     */
    public void verbose(String output, Object... args) {
        this.verbose(String.format(output, args));
    }

    /**
     * Logs if level is set to verbose or more
     * Takes args like String.format
     */
    public void verbose(String output) {
        if (shouldLog(VERBOSE)) {
            System.out.println(LOG_INDICATOR + output);
        }
    }

    /**
     * Logs if level is set to quiet or more
     * Takes args like String.format
     */
    public void quiet(String output, Object... args) {
        this.verbose(String.format(output, args));
    }

    /**
     * Logs if level is set to quiet or more
     */
    public void quiet(String output) {
        if (shouldLog(QUIET)) {
            System.out.println(LOG_INDICATOR + output);
        }
    }

    private boolean shouldLog(Level level) {
        return level.ordinal() >= config.getLogLevel().ordinal();
    }

    public enum Level {
        DEBUG, VERBOSE, QUIET, SILENT
    }
}
