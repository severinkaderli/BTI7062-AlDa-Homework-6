package benchmark.utils.io;

import benchmark.utils.config.ConfigBuilder;
import benchmark.utils.DataProvider;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Reads the options provided in the ConfigBuilder via Reflection
 * and accepts them as CLI arguments in the format:
 * --option=value
 */
public class ArgParser {
    private final Map<String, Class> options;

    private final ConfigBuilder builder;

    public ArgParser() {
        builder = new ConfigBuilder();
        options = extractOptions(builder);
    }

    private Map<String, Class> extractOptions(ConfigBuilder cfgBuilder){
        Map<String, Class> opts = new HashMap<>();
        Method[] methods = builder.getClass().getMethods();
        opts.putAll(Stream.of(methods)
                .filter(m -> m.getDeclaringClass().equals(cfgBuilder.getClass()))
                .filter(m -> !m.getName().startsWith("build"))
                .collect(Collectors.toMap(Method::getName, m -> m.getParameterTypes()[0])));
        return opts;
    }

    /**
     * Pass the java args to this to parse them.
     */
    public ConfigBuilder parse(String[] args) {
        if (args.length == 0) {
            return builder;
        } else if(args.length == 1 && (args[0].equals("--help") || args[0].equals("-h"))){
            System.out.println("Help for QuickSort Benchmarker:");
            for (Map.Entry<String, Class> e : options.entrySet()){
                System.out.printf("--%s=%s%n", e.getKey(), createHelp(e.getValue()));
            }
            System.exit(0);
        }

        boolean printArgs = false;

        for (String arg : args) {
            String option = getOption(arg);
            String value = getValue(arg);
            if(option.equals("printArgs")){
                printArgs = Boolean.valueOf(value);
                continue;
            }
            if (!options.containsKey(option)) {
                System.out.println("Unknown arg, Try these:");
                for (String o : options.keySet()) {
                    System.out.println("\t" + o);
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                throw new IllegalArgumentException("Unknown option: " + arg);
            }
            Class cls = options.get(option);
            if (Integer.class.equals(cls)) {
                setOption(option, cls, parseInt(value));
            } else if (List.class.equals(cls)) {
                setOption(option, cls, parseList(value));
            } else if (DataProvider.Case.class.equals(cls)) {
                setOption(option, cls, parseCase(value));
            } else if (Boolean.class.equals(cls)) {
                setOption(option, cls, parseBoolean(value));
            } else if (Logger.Level.class.equals(cls)) {
                setOption(option, cls, parseLogLevel(value));
            }
        }

        if(printArgs){
            printArgs();
        }

        return builder;
    }

    private void printArgs(){
        System.out.println("Running with following config:");
        try{
            for(Field f : builder.getClass().getDeclaredFields()){
                f.setAccessible(true);
                System.out.printf("\t%s = %s%n", f.getName(), f.get(builder).toString());
            }
        }catch (IllegalAccessException ex){
            //NOP, swallow
        }
    }

    private String createHelp(Class type){
        if(type.isEnum()){
            Field[] fields = type.getFields();
            return String.format("[%s]", Stream.of(fields)
                    .map(Field::getName)
                    .collect(Collectors.joining(",")));
        }else if(type.isAssignableFrom(List.class)){
            return "List (comma seperated Integers)";
        }
        return type.getSimpleName();
    }

    private Object parseInt(String input) {
        return Integer.parseInt(input);
    }

    private Object parseList(String input) {
        List<Integer> list = new ArrayList<>();
        for (String s : input.split(",")) {
            list.add(Integer.parseInt(s));
        }
        return list;
    }

    private Object parseCase(String input) {
        return DataProvider.Case.valueOf(input.toUpperCase());
    }

    private Object parseBoolean(String input) {
        return Boolean.parseBoolean(input);
    }

    private Object parseLogLevel(String input) {
        return Logger.Level.valueOf(input.toUpperCase());
    }

    private String getOption(String input) {
        return input.split("=")[0].substring(2);
    }

    private String getValue(String input) {
        return input.split("=")[1];
    }

    private void setOption(String option, Class paramClass, Object paramValue) {
        try {
            Method method = this.builder.getClass().getMethod(option, paramClass);
            method.invoke(this.builder, paramClass.cast(paramValue));
        } catch (Exception ex) {
            System.out.println("Something went horribly wrong trying to use reflection");
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
