package benchmark.utils;

import benchmark.utils.config.Config;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class DataProvider {
    private List<Long> all;
    private Map<Integer, List<Long>> cache;
    private Config config;

    private static Map<Config, DataProvider> instances = new HashMap<>();

    public static DataProvider ensure(Config config) {
        return instances.computeIfAbsent(config, DataProvider::new);
    }

    private DataProvider(Config config) {
        this.config = config;
        this.all = LongStream.rangeClosed(1L, this.config.getUpperBound()).boxed().collect(Collectors.toList());
        this.cache = new HashMap<>(this.config.getUpperBound() / this.config.getStepSize());

        switch (this.config.getBenchmarkCase()) {
            case BEST:
                //NOP, already generated best case
                break;
            case AVERAGE:
                if (this.config.getRandomSeed() != -1) {
                    Collections.shuffle(all, new Random(this.config.getRandomSeed()));
                }
                Collections.shuffle(all, new Random());
                break;
            case WORST:
                Collections.reverse(all);
                break;
        }
    }

    public List<Long> get(int amount) {
        return new ArrayList<>(cache.computeIfAbsent(amount, i -> all.subList(0, i)));
    }

    public enum Case {
        BEST, AVERAGE, WORST
    }
}
