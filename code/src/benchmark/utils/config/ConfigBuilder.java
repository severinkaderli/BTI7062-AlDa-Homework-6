package benchmark.utils.config;

import benchmark.utils.DataProvider;
import benchmark.utils.io.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConfigBuilder {
    private int lowerBound = 0;
    private int upperBound = 750_000;
    private int stepSize = 100_000;
    private int numberOfWarmupIterations = 2;
    private List<Integer> threadCounts = new ArrayList<>(Arrays.asList(2, 4));
    private DataProvider.Case benchmarkCase = DataProvider.Case.WORST;
    private int randomSeed = 1337;
    private boolean checkCorrectness = false;
    private Logger.Level logLevel = Logger.Level.SILENT;

    public ConfigBuilder() {
        // default constructor
    }

    public Config build() {
        return new Config(
                lowerBound,
                upperBound,
                stepSize,
                numberOfWarmupIterations,
                threadCounts,
                benchmarkCase,
                randomSeed,
                checkCorrectness,
                logLevel
        );
    }

    public QuickSortConfig buildQuickSortConfig() {
        return new QuickSortConfig(
                lowerBound,
                upperBound,
                Math.min(stepSize, upperBound),
                numberOfWarmupIterations,
                threadCounts,
                benchmarkCase,
                randomSeed,
                checkCorrectness,
                logLevel
        );
    }

    public ConfigBuilder setLowerBound(Integer lowerBound) {
        this.lowerBound = lowerBound;
        return this;
    }

    public ConfigBuilder setUpperBound(Integer upperBound) {
        this.upperBound = upperBound;
        return this;
    }

    public ConfigBuilder setStepSize(Integer stepSize) {
        this.stepSize = stepSize;
        return this;
    }

    public ConfigBuilder setNumberOfWarmupIterations(Integer numberOfWarmupIterations) {
        this.numberOfWarmupIterations = numberOfWarmupIterations;
        return this;
    }

    public ConfigBuilder setThreadCounts(List<Integer> threadCounts) {
        this.threadCounts = threadCounts;
        return this;
    }

    public ConfigBuilder setBenchmarkCase(DataProvider.Case benchmarkCase) {
        this.benchmarkCase = benchmarkCase;
        return this;
    }

    public ConfigBuilder setRandomSeed(Integer randomSeed) {
        this.randomSeed = randomSeed;
        return this;
    }

    public ConfigBuilder setCheckCorrectness(Boolean checkCorrectness) {
        this.checkCorrectness = checkCorrectness;
        return this;
    }

    public ConfigBuilder setLogLevel(Logger.Level logLevel) {
        this.logLevel = logLevel;
        return this;
    }
}

