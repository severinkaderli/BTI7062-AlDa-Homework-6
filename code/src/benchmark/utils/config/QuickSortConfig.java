package benchmark.utils.config;

import algorithms.quicksort.strategies.PivotChoiceStrategy;
import algorithms.quicksort.strategies.Strategies;
import benchmark.utils.DataProvider;
import benchmark.utils.io.Logger;

import java.util.List;

public class QuickSortConfig extends Config {

    protected QuickSortConfig(int lowerBound, int upperBound, int stepSize, int numberOfWarmupIterations, List<Integer> threadCounts, DataProvider.Case benchmarkCase, int randomSeed, boolean checkCorrectness, Logger.Level logLevel) {
        super(lowerBound, upperBound, stepSize, numberOfWarmupIterations, threadCounts, benchmarkCase, randomSeed, checkCorrectness, logLevel);
    }

    /**
     * Strategies to use for the basecase and recombination strategy.
     * BaseCase:
     * Choose either BaseCase (if input.size less than 2, it's implicitly sorted, return input)
     * or InsertionSortBaseCase (if input.size less/equal to 10, use InsertionSort and return the sorted list)
     * Recombine:
     * Choose either RecombineStrategy (The "default mergesort strategy, as described on Wikipedia)
     * or InsertionSortRecombineStrategy (Appends the subcomponents and uses insertionsort on that list)
     */
    public Strategies getStrategies() {
        return new Strategies(PivotChoiceStrategy.INSTANCE);
    }
}
