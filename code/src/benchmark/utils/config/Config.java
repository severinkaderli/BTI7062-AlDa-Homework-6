package benchmark.utils.config;

import benchmark.utils.DataProvider;
import benchmark.utils.io.Logger;

import java.util.List;

public class Config {

    private final int lowerBound;
    private final int upperBound;
    private final int stepSize;
    private final int numberOfWarmupIterations;
    private final List<Integer> threadCounts;
    private final DataProvider.Case benchmarkCase;
    private final int randomSeed;
    private final boolean checkCorrectness;
    private final Logger.Level logLevel;

    //only for builder
    protected Config(int lowerBound, int upperBound, int stepSize,
                     int numberOfWarmupIterations, List<Integer> threadCounts,
                     DataProvider.Case benchmarkCase, int randomSeed,
                     boolean checkCorrectness, Logger.Level logLevel) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.stepSize = stepSize;
        this.numberOfWarmupIterations = numberOfWarmupIterations;
        this.threadCounts = threadCounts;
        this.benchmarkCase = benchmarkCase;
        this.randomSeed = randomSeed;
        this.checkCorrectness = checkCorrectness;
        this.logLevel = logLevel;
    }

    /**
     * The lower bound of the numbers which will
     * be sorted.
     */
    public int getLowerBound() {
        return lowerBound;
    }

    /**
     * The upper bound of the numbers which will
     * be sorted.
     */
    public int getUpperBound() {
        return upperBound;
    }

    /**
     * Stept to increase numbers to sort by
     */
    public int getStepSize() {
        return stepSize;
    }

    /**
     * Number of warmup iterations
     */
    public int getNumberOfWarmupIterations() {
        return numberOfWarmupIterations;
    }

    /**
     * The thread counts which should be used for the calculations.
     * The application will sequentially test with the threadcounts given here.
     */
    public List<Integer> getThreadCounts() {
        return threadCounts;
    }

    /**
     * The case for which we should generate Data [BEST, AVERAGE, WORST].
     * Best:    the list will already be sorted
     * Average: the list is shuffled randomly
     * Worst:   the list is sorted backwards
     */
    public DataProvider.Case getBenchmarkCase() {
        return benchmarkCase;
    }

    /**
     * The seed to use for any randomness.
     * Set to -1 for using using unparameterized random
     */
    public int getRandomSeed() {
        return randomSeed;
    }

    /**
     * Checks the generated results for correctness.
     * WARNING! This has a performance impact and measurements are invalid.
     */
    public boolean getCheckCorrectness() {
        return checkCorrectness;
    }

    /**
     * Sets how verbose the application should be in its logging.
     */
    public Logger.Level getLogLevel() {
        return logLevel;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getLowerBound();
        result = prime * result + getUpperBound();
        result = prime * result + getStepSize();
        result = prime * result + getNumberOfWarmupIterations();
        result = prime * result + getRandomSeed();
        result = prime * result + getThreadCounts().hashCode();
        result = prime * result + getBenchmarkCase().ordinal();
        result = prime * result + (getCheckCorrectness() ? 1231 : 1237);
        return result;
    }
}
