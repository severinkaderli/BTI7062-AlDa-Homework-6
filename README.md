# BTI7062-AlDa-Homework-6
## Task
Reprogram Quicksort by implementing our single- & multi-threaded DnC-interfaces (like Mergesort), and compare the performances graphically.

## ZIP File
The ready to submit zip file can be found [here](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-5/builds/artifacts/master/raw/TEAM-Kaderli-Kilic-Schaer.zip?job=deploy).
